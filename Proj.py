import telebot
import json
from bs4 import BeautifulSoup
import numpy as np
import pandas as pd
import requests
import os
import zipfile
import time
from datetime import datetime, date, time

TOKEN = '494753784:AAHaSr8O0siGvvZ9mK6yooJV7yMS9V-p7yA'
bot = telebot.TeleBot(TOKEN)
@bot.message_handler(commands=['start'])
def start(message):
    sent = bot.send_message(message.chat.id, 'Стартанул.')
    url = "http://coincap.io/coins/"
    response = requests.get(url, timeout=1000)
    soup = BeautifulSoup(response.content, "html.parser")
    array_of_all_coins = json.loads(soup.prettify())

    array_of_all_coins.remove('CON')

    sent = bot.send_message(message.chat.id,"In total " + str(len(array_of_all_coins)) + " coins")

    iterator = 0;
    nuber_of_wrong_currencies = 0;
    for coin in array_of_all_coins:
        if str(coin) == '888':
            break
        iterator = iterator + 1
        print(iterator)
        url = "http://coincap.io/history/" + str(coin)
        response = requests.get(url, timeout=1000)
        soup = BeautifulSoup(response.content, "html.parser")
        dic = json.loads(soup.prettify())

        labels1 = ['timestamp', 'market_cap']
        labels2 = ['timestamp', 'price']
        if type(dic) == type(dict()):
            df1 = pd.DataFrame.from_records(dic['market_cap'], columns=labels1)
            df2 = pd.DataFrame.from_records(dic['price'], columns=labels2)
            res = df1.merge(df2, 'left', on='timestamp')
            res.to_csv(str(coin) + ".csv")
        else:
            sent = bot.send_message(message.chat.id, "no information about " + str(coin) + " coin")
            nuber_of_wrong_currencies = nuber_of_wrong_currencies + 1
            continue
    archive_name = datetime.strftime(datetime.now(), "%Y.%m.%d")
    #archive_name = datetime.date()
    sent = bot.send_message(message.chat.id,"Wrong in total: " + str(nuber_of_wrong_currencies))
    fantasy_zip = zipfile.ZipFile('/home/evgenii/telegram-project/' + str(archive_name) + '.zip', 'w')

    for folder, subfolders, files in os.walk('/home/evgenii/telegram-project/'):
        for file in files:
            if file.endswith('.csv'):
                fantasy_zip.write(os.path.join(folder, file), os.path.relpath(os.path.join(folder,file), '/home/evgenii/telegram-project/'), compress_type = zipfile.ZIP_DEFLATED)
    fantasy_zip.close()
    doc = document=open('/home/evgenii/telegram-project/' + str(archive_name) + '.zip', 'rb')
    bot.send_document(message.chat.id, doc)

bot.polling()
